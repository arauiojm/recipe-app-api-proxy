#!bin/sh

set -e #se algo falhar retorna um failure

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;'